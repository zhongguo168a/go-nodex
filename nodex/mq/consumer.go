package mq

import (
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"gitee.com/zhongguo168a/gocodes/myx/logx"
	"github.com/nsqio/go-nsq"
)

func CreateConsumer(lookupAddr string, topic, channel string, config *nsq.Config, handler nsq.HandlerFunc) (consumer *nsq.Consumer, err error) {
	consumer, err = nsq.NewConsumer(topic, channel, config)
	if err != nil {
		err = errorx.Wrap(err, "nsq.NewConsumer", datax.M{"topic": topic, "channel": channel})
		return
	}

	consumer.AddHandler(handler)

	go func() {
		err = consumer.ConnectToNSQLookupd(lookupAddr)
		if err != nil {
			err = errorx.Wrap(err, "nsq.ConnectToNSQLookupd", datax.M{"topic": topic, "channel": channel, "addr": lookupAddr})
			logx.Fatal(err)
		}
	}()

	return
}
