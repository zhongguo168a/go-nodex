package nodex

import (
	"github.com/pkg/errors"
	"strings"
)

type IRouteGroup interface {
}

type RouteGroupConfig struct {
	// 添加到的服务器
	AtServers []string
	// 路由组的名字
	Name string
	// 使用CHANRPC模式时，指定的选择器
	RPCSelector string
	// 调用模式
	CallMode CallMode
	// 使用的上下文创建器
	UseContextCreator ContextCreator
	// 处理器
	RouteHandler IRouteGroup
	// 当使用http服务器时，开启HTTP GET模式
	EnableHttpGet bool
}

func NewRouteGroup(conf *RouteGroupConfig) (obj *RouteGroup, err error) {
	obj = &RouteGroup{}
	obj.RouteGroupConfig = conf

	if conf.Name == "" {
		err = errors.New("server selector name is nil")
		return
	}

	arr := strings.Split(conf.Name, "/")
	if len(arr) > 0 {
		obj.rpcName = arr[0]
	}
	return
}

// RouteGroup 用一个结构体来定义一组路由
type RouteGroup struct {
	*RouteGroupConfig
	// 路由的第一个值默认为rpc服务器
	rpcName string
}

func (n *Node) getRouteGroupByRPC(rpc string) (r []*RouteGroup) {
	for _, val := range n.routeGroupMap {
		if val.Name == rpc {
			r = append(r, val)
		}
	}

	return
}
