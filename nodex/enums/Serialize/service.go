package Serialize

type Mode int8

const (
	JSON Mode = iota
	BYTE
)
