package memdb

import (
	"github.com/songangweb/mcache"
	"time"
)

var cache *mcache.HashLruCache

func Init() (err error) {
	cache, err = mcache.NewHashLRU(9223372036854775807, 0)
	if err != nil {
		return
	}

	return
}

func Put(key interface{}, value interface{}) {
	cache.Add(key, value, 0)
}

func PutExpired(key interface{}, value interface{}, expirationTime time.Duration) {
	cache.Add(key, value, int64(expirationTime)/1e6)
}

func Get(key interface{}) (value interface{}, has bool) {
	value, _, has = cache.Get(key)
	return value, has
}
