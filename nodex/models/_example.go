package models

import (
	"gitee.com/zhongguo168a/go-nodex/dbx/mgoz"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func init() {
	// 如果需要在数据库中保存，需要定义动态数据[do]
	dbz.RegisterTable(&mgoz.Table{
		Name:       "example",
		Database:   "nodex",
		PrimaryKey: "_id",
		Indexs: map[int]*mongo.IndexModel{
			0: {
				Keys: bson.M{"Name": "_id", "Sequence": 1},
			},
		},
	})
}

func NewExample() (obj *Example) {
	obj = &Example{}
	return
}

type Example struct {
	Id primitive.ObjectID `json:"_id"`
}

func (e *Example) DBTable() string {
	return "example"
}

func (e *Example) DBIdent() string {
	return e.Id.String()
}

func (e *Example) DBQuery() map[string]interface{} {
	return map[string]interface{}{
		"_id": e.Id,
	}
}

// 获取用于数据库访问的对象
func (e *Example) DBObject() interface{} {
	return e
}

func (e *Example) DBInit() (err error) {
	return
}

/** ***********************************************************
  ==============================================================
  服务器
  ==============================================================
*/
type ExampleList struct {
	List arrayutil.AnyList
}

func (list *ExampleList) DBTable() string {
	return "database"
}

func (list *ExampleList) DBNewModel() (r dbz.IModel) {
	r = NewExample()
	list.List.Add(r)
	return
}

func (list *ExampleList) DBQuery() map[string]interface{} {
	return map[string]interface{}{}
}
