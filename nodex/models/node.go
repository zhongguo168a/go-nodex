package models

import (
	"gitee.com/zhongguo168a/go-nodex/dbx"
	"gitee.com/zhongguo168a/go-nodex/dbx/mongox"
	"gitee.com/zhongguo168a/gocodes/datax/listx/constraints"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const TableNode = "node"

func init() {
	dbx.RegisterTable(&mongox.Table{
		Name:       TableNode,
		Database:   NODE_DBNAME,
		PrimaryKey: "_id",
		Indexs: map[int]*mongo.IndexModel{
			0: {
				Keys: bson.D{
					{Key: "_id", Value: mongox.IndexSortOrderAscende},
				},
			},
		},
	})
}

type NodeState string

const (
	Stop     NodeState = "Stop"
	Starting NodeState = "Starting"
	Runing   NodeState = "Runing"
)

func NewNode() (obj *Node) {
	obj = &Node{}
	return
}

type Node struct {
	Id         string `alias:"_id"`
	State      NodeState
	Error      string
	UpdateTime int
	CreateIime int
	Map        map[string]interface{}
}

func (e *Node) DBTable() string {
	return TableNode
}

func (e *Node) DBIdent() string {
	return e.Id
}

func (e *Node) DBQuery() map[string]interface{} {
	return map[string]interface{}{
		"_id": e.Id,
	}
}
func (e *Node) DBQueryName(id string) map[string]interface{} {
	return map[string]interface{}{
		"_id": id,
	}
}

func (e *Node) DBObject() interface{} {
	return e
}

func (e *Node) DBInit() (err error) {
	return
}

type NodeList struct {
	constraints.List[*Node]
}

func (list *NodeList) DBTable() string {
	return TableNode
}

func (list *NodeList) DBQueryAll() dbx.IDBModel {
	r := NewNode()
	list.List.Add(r)
	return r
}

func (list *NodeList) DBQuery() map[string]interface{} {
	return map[string]interface{}{}
}
