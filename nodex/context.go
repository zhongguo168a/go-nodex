package nodex

import (
	"context"
	"gitee.com/zhongguo168a/go-nodex/nodex/server"
	"sync"
)

// IContext 定义连接接口
type IContext interface {
	context.Context
	// GetRequest 响应该路由的请求
	GetRequest() server.IRequest
	// GetConnection 响应该路由的连接
	GetConnection() server.IConnection
	// GetServer 响应该路由的服务器
	GetServer() IServer
	// GetRPC 如果当前处于 chanrpc，则返回该 chanrpc
	GetRPC() IChanRPC
	//
	GetSync() ISyncConn
	// 获取链接的属性集合
	GetProperty() *sync.Map
}
type ContextCreator func(ctx IContext) (newctx IContext, err error)

type Context struct {
	context.Context

	conn   server.IConnection
	requet *server.Request
	rpc    IChanRPC
	server IServer
	syncer ISyncConn
}

//获取链接属性
func (c *Context) GetProperty() *sync.Map {
	return c.conn.GetProperty()
}

func (ctx *Context) GetServer() IServer {
	return ctx.server
}

func (ctx *Context) GetRPC() IChanRPC {
	return ctx.rpc
}

func (ctx *Context) GetRequest() server.IRequest {
	return ctx.requet
}

func (ctx *Context) GetConnection() server.IConnection {
	return ctx.conn
}

func (ctx *Context) GetSync() ISyncConn {
	if ctx.syncer == nil {
		ctx.syncer = NewSyncConn(ctx.conn)
	}
	return ctx.syncer
}
