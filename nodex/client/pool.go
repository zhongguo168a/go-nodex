package client

import (
	"gitee.com/zhongguo168a/gocodes/myx/pool"
	"net"
)

var pools = pool.NewPoolMap()

func GetClient(address string) (client *Client, err error) {
	if pools.Count(address) == 0 {
		conn, dailerr := net.Dial("tcp", address)
		if dailerr != nil {
			return nil, dailerr
		}
		client = NewClientByConn(address, conn)
		return
	}

	client = pools.Get(address).(*Client)
	if client.shutdown || client.closing {
		// 新建一个
		conn, dailerr := net.Dial("tcp", address)
		if dailerr != nil {
			return nil, dailerr
		}
		client = NewClientByConn(address, conn)
	}

	return client, nil
}

func putClient(client *Client) {
	client.token = ""
	pools.Put(client.address, client)
}
