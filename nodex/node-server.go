package nodex

import (
	"gitee.com/zhongguo168a/go-nodex/nodex/models"
	"gitee.com/zhongguo168a/go-nodex/nodex/server"
	"sync"
)

var serverCreator = &sync.Map{}

func RegisterServerCreator(mode string, f func(conf *models.ServerConfig) server.IServer) {
	serverCreator.Store(mode, f)
}

// 服务器
// 服务器提供客户端连接入口，支持多种模式
type IServer interface {
	server.IServer
}

func newServerSet() (obj *serverSet) {
	obj = &serverSet{
		data: &sync.Map{},
	}
	return
}

type serverSet struct {
	data *sync.Map
}

func (set *serverSet) Get(ident string) IServer {
	srv, ok := set.data.Load(ident)
	if ok == false {
		return nil
	}
	return srv.(IServer)
}

func (set *serverSet) Add(val IServer) {
	set.data.Store(val.Name(), val)
}

func (set *serverSet) Remove(name string) {
	set.data.Delete(name)
}
