package nodex

import (
	"gitee.com/zhongguo168a/go-nodex/dbx/mongox"
	"gitee.com/zhongguo168a/go-nodex/nodex/models"
)

type SerializeMode int

const (
	JSON SerializeMode = iota
	BYTE
)

// NodeConfig 节点配置
type NodeConfig struct {
	// 节点的名字
	// 例如中心服务器，游戏服务器，日志服务器
	Name string
	// nodex专用的数据库，用于保存节点，数据库，服务器信息
	Database *mongox.Database
	// 保存成文件的kv数据库
	FileDBPath string
	// 例如该节点属于某个渠道
	Channel string
	// 序列化方式 0-json 1-byte
	SerializeMode SerializeMode
	// 节点开启的服务器
	// 地址在nodex数据库中修改
	Servers []*models.ServerConfig
	// 启动服务前的操作
	// 返回错误终止进程
	PreServe func() (err error)
	// ChanRPCSelectors
	// ChanRPC name -> 选择器
	// 内部的rpc需要手动创建，例如玩家rpc，当玩家登陆的时候创建
	ChanRPCSelectors map[string]ChanRPCSelector
	// 协议路由
	Routes []*RouterConfig
	// 定义服务配置
	RouteGroup []*RouteGroupConfig
	// 消息队列配置
	MsgQueue interface{}
}

type NsqMsgQueue struct {
	// 消息队列地址
	ProducerAddr string
	// 消息队列地址
	LookupdAddr string
}
