package nodex

type ProtoEmptyArgs struct {
}

// 用于处理Http协议时，直接返回字符串
// 场景：处理渠道SDK时使用
type HttpResultString struct {
	Data string
}

type ProtoEmptyResult struct {
}

type ProtoNoResult struct {
}
