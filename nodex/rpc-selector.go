package nodex

// ChanRPCSelector chanrpc的选择器
// 用于设定选择chanrpc的规则
type ChanRPCSelector func(ctx IContext, route *Router) (rpckey string, err error)

func defaultGetRPCHandle(ctx IContext, route *Router) (rpcKey string, err error) {
	return route.GroupName, nil
}
