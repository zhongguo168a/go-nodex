package ws

import (
	"bytes"
	"context"
	"fmt"
	"gitee.com/zhongguo168a/go-nodex/nodex/server"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"gitee.com/zhongguo168a/gocodes/myx/logx"
	"github.com/gorilla/websocket"
	"net"
	"sync"
	"time"
)

type Connection struct {
	//当前Conn属于哪个Server
	Server server.IServer
	//当前连接的socket TCP套接字
	Conn *websocket.Conn
	//当前连接的ID 也可以称作为SessionID，ID全局唯一
	ConnID uint64
	//消息管理MsgId和对应处理方法的消息管理模块
	MsgHandler server.IMsgHandle
	//告知该链接已经退出/停止的channel
	ctx    context.Context
	cancel context.CancelFunc
	//无缓冲管道，用于读、写两个goroutine之间的消息通信
	msgChan chan []byte
	//有缓冲管道，用于读、写两个goroutine之间的消息通信
	msgBuff []server.IMessage

	sync.RWMutex
	//链接属性
	property *sync.Map
	//当前连接的关闭状态
	isClosed bool
	//8
	lastHeartBeatTime time.Time
}

// 创建连接的方法
func NewConntion(server server.IServer, conn *websocket.Conn, connID uint64, msgHandler server.IMsgHandle) *Connection {
	//初始化Conn属性
	c := &Connection{
		Server:     server,
		Conn:       conn,
		ConnID:     connID,
		isClosed:   false,
		MsgHandler: msgHandler,
		msgChan:    make(chan []byte),
		property:   &sync.Map{},
	}

	//将新创建的Conn添加到链接管理中
	c.Server.GetConnMgr().Add(c)
	return c
}

/*
写消息Goroutine， 用户将数据发送给客户端
*/
func (c *Connection) StartWriter() {
	var (
		err error
	)
	defer func() {
		if err != nil {
			logx.Info(fmt.Sprintf("connection closed at writer: %v", err.Error()))
		}
		c.Stop()
	}()

	for {
		select {
		case data := <-c.msgChan:
			//有数据要写给客户端
			if msgerr := c.Conn.WriteMessage(websocket.BinaryMessage, data); err != nil {
				err = errorx.Wrap(msgerr, "write message")
				return
			}
			//fmt.Printf("Send data succ! data = %+v\n", data)
		case <-c.ctx.Done():
			return
		}
	}
}

func (c *Connection) startFlush() {
	var (
		err error
	)
	defer func() {
		if err != nil {
			logx.Info(fmt.Sprintf("connection closed at flush: %v", err.Error()))
		}
		c.Stop()
	}()

	for {
		errFlush := c.Flush()
		if errFlush != nil {
			logx.Error(fmt.Sprintf("flush: %v", err.Error()))
		}
		// 没毫秒检测一次，
		time.Sleep(time.Millisecond)
	}

}

// StartReader 读消息Goroutine，用于从客户端中读取数据
func (c *Connection) StartReader() {
	var (
		err error
	)
	defer func() {
		if err != nil {
			logx.Info(fmt.Sprintf("connection closed at reader: %v", err.Error()))
		}
		c.Stop()
	}()

	for {
		select {
		case <-c.ctx.Done():
			return
		default:
			// 创建拆包解包的对象
			_, received, msgerr := c.Conn.ReadMessage()
			if msgerr != nil {
				err = errorx.Wrap(msgerr, "read message")
				return
			}
			reader := bytes.NewReader(received)
			req := &server.Request{
				Conn: c,
			}
			if unpackerr := server.UnpackReceivedData(reader, c.Server, c.MsgHandler, req); unpackerr != nil {
				err = errorx.Wrap(unpackerr, "UnpackReceivedData")
				return
			}
			//从绑定好的消息和对应的处理方法中执行对应的Handle方法
			doerr := c.MsgHandler.Do(req)
			if doerr != nil {
				err = errorx.Wrap(doerr, "do message")
				return
			}
		}
	}

}

// 启动连接，让当前连接开始工作
func (c *Connection) Start() {
	c.ctx, c.cancel = context.WithCancel(context.Background())
	//1 开启用户从客户端读取数据流程的Goroutine
	go c.StartReader()
	//2 开启用于写回客户端数据流程的Goroutine
	go c.StartWriter()
	//3 todo: 同步到tcp
	go c.startFlush()
	//按照用户传递进来的创建连接时需要处理的业务，执行钩子方法
	c.Server.CallOnConnStart(c)
}

// 停止连接，结束当前连接状态M
func (c *Connection) Stop() {
	//fmt.Println("Conn Stop()...ConnID = ", c.ConnID)

	c.Lock()
	defer c.Unlock()

	//如果当前链接已经关闭
	if c.isClosed == true {
		return
	}
	c.isClosed = true
	//如果用户注册了该链接的关闭回调业务，那么在此刻应该显示调用
	c.Server.CallOnConnStop(c)

	// 关闭socket链接
	c.Conn.Close()
	//关闭Writer
	c.cancel()

	//将链接从连接管理器中删除
	c.Server.GetConnMgr().Remove(c)

	//关闭该链接全部管道
	close(c.msgChan)
}

// 从当前连接获取原始的socket
func (c *Connection) GetConnection() *websocket.Conn {
	return c.Conn
}

// 获取当前连接ID
func (c *Connection) GetConnID() uint64 {
	return c.ConnID
}

// 获取远程客户端地址信息
func (c *Connection) RemoteAddr() net.Addr {
	return c.Conn.RemoteAddr()
}

// 直接将Message数据发送数据给远程的TCP客户端
func (c *Connection) SendMsg(message server.IMessage) error {
	c.RLock()
	if c.isClosed == true {
		c.RUnlock()
		return errorx.New("connection closed when send msg")
	}
	c.RUnlock()

	//将data封包，并且发送
	buff := bytes.NewBuffer([]byte{})
	err := message.Pack(buff)
	if err != nil {
		return errorx.Wrap(err, "pack")
	}

	//写回客户端
	c.msgChan <- buff.Bytes()
	return nil
}

func (c *Connection) SendMsgBuff(message server.IMessage) error {
	c.RLock()
	if c.isClosed == true {
		c.RUnlock()
		return errorx.New("connection closed when send buff msg")
	}

	c.msgBuff = append(c.msgBuff, message)
	c.RUnlock()

	return nil
}

// 清空缓冲区数据
func (c *Connection) Flush() error {
	if len(c.msgBuff) == 0 {
		return nil
	}

	//将data封包，并且发送
	buff := bytes.NewBuffer([]byte{})
	c.Lock()
	for _, message := range c.msgBuff {
		err := message.Pack(buff)
		if err != nil {
			return errorx.Wrap(err, "pack")
		}
	}
	c.msgBuff = c.msgBuff[:0]
	c.Unlock()
	//写回客户端
	c.msgChan <- buff.Bytes()
	return nil
}

// 获取链接属性
// 可通过mapsync访问属性
func (c *Connection) GetProperty() *sync.Map {
	return c.property
}
