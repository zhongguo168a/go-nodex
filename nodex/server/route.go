package server

type IRouter interface {
	GetEnableHttpGet() bool
	NewArgs() (args interface{}, err error)
	NewReply() (obj interface{}, err error)
}
