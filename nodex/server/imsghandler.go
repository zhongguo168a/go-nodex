package server

type IMsgHandle interface {
	Do(request *Request) (err error)
	AddRouter(msgId string, router IRouter)
	GetRoute(key string) IRouter
	GetRouters() map[string]IRouter
}
