package server

import "github.com/gin-gonic/gin"

/*
IRequest 接口：
实际上是把客户端请求的链接信息 和 请求的数据 包装到了 Request里
*/
type IRequest interface {
	GetConnection() IConnection //获取请求连接信息
	GetData() IRequestMessage   //获取请求消息的数据
	GetToken() string
	//获取请求的消息ID
	//GetMsg() *RequestMessage

	GetHttpContext() *gin.Context
}

type IRequestMessage interface {
	GetMsgID() string
	GetToken() string
}

type Request struct {
	Conn    IConnection     //已经和客户端建立好的链接，如果是http服务器和rpc服务器的请求，则为null
	Context interface{}     // http的上下文
	Message IRequestMessage //客户端请求的数据
	Args    interface{}
	Reply   interface{}
	Route   IRouter
	Token   string
}

func (r *Request) GetToken() string {
	return r.Token
}

// 获取请求连接信息
func (r *Request) GetConnection() IConnection {
	return r.Conn
}

func (r *Request) GetHttpContext() *gin.Context {
	return r.Context.(*gin.Context)
}

// 获取请求消息的数据
func (r *Request) GetData() IRequestMessage {
	return r.Message
}
