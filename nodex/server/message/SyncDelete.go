package message

import (
	"encoding/binary"
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/datax/binaryx"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"io"
)

// 服务器推送消息
type SyncDelete struct {
	Key string
}

func (msg *SyncDelete) Unpack(reader io.Reader) error {
	if data, err := binaryx.ReadUTF(reader, binary.LittleEndian); err != nil {
		return errorx.Wrap(err, "read Key")
	} else {
		msg.Key = data
	}
	return nil
}

func (msg *SyncDelete) PackageLen() int {
	// PushTag + Key
	return 1 + (2 + len(msg.Key))
}

//封包方法(压缩数据)
func (msg *SyncDelete) Pack(writer io.Writer) error {
	if err := binary.Write(writer, binary.LittleEndian, int16(msg.PackageLen())); err != nil {
		return errorx.Wrap(err, "write PackageLen", datax.M{"PackageLen": msg.PackageLen()})
	}
	// tag data
	if err := binary.Write(writer, binary.LittleEndian, int8(PushTag_SyncDelete)); err != nil {
		return errorx.Wrap(err, "write PushTag", datax.M{"PushTag": int8(PushTag_SyncDelete)})
	}

	if err := binaryx.WriteUTF(writer, binary.LittleEndian, msg.Key); err != nil {
		return errorx.Wrap(err, "write Key", datax.M{"Key": msg.Key})
	}

	return nil
}
