package message

// 请求消息
type HttpMessage struct {
	//
	MsgId string //消息的ID
	// http模式需要每次请求都带上Token
	Token string
	//
	Data map[string]interface{} //消息的内容
}

func (msg *HttpMessage) GetMsgID() string {
	return msg.MsgId
}
func (msg *HttpMessage) GetToken() string {
	return msg.Token
}
