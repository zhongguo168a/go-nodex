package message

//type ReceiveTag int
//
//const (
//	ReceiveTag_Unknown ReceiveTag = iota
//	ReceiveTag_Request
//)

type PushTag int

const (
	PushTag_Unknown PushTag = iota
	PushTag_Response
	// 推送原始数据
	PushTag_Push
	PushTag_Event
	PushTag_Command
	PushTag_SyncCreate
	PushTag_SyncUpdate
	PushTag_SyncDelete
)
