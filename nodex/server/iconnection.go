package server

import (
	"net"
	"sync"
)

// 定义连接接口
type IConnection interface {
	//启动连接，让当前连接开始工作
	Start()
	//停止连接，结束当前连接状态M
	Stop()

	//获取当前连接ID
	GetConnID() uint64
	//获取远程客户端地址信息
	RemoteAddr() net.Addr

	//直接将Message数据发送数据给远程的TCP客户端(无缓冲)
	SendMsg(message IMessage) error
	//直接将Message数据发送给远程的TCP客户端(有缓冲)
	SendMsgBuff(message IMessage) error
	// 清空缓冲区数据
	Flush() error
	//获取链接属性
	GetProperty() *sync.Map
}
