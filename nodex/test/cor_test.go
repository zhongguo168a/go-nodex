package test

import (
	"fmt"
	"gitee.com/zhongguo168a/gocodes/gox/osx/signalx"
	"sync"
	"testing"
	"time"
)

type A struct {
	IntProp int
	B       *B
}

type B struct {
	IntProp int
}

func TestName(t *testing.T) {

	a := &A{
		B: &B{},
	}

	m := map[int]interface{}{}
	for i := 0; i < 1000000; i++ {
		m[i] = a.IntProp
	}
	go func() {
		for i := 0; i < 1000000; i++ {
			a.IntProp = i
			a.B = &B{}
		}
	}()
	go func() {
		for i := 0; i < 1000000; i++ {
			a.IntProp = i
			a.B = nil
		}
	}()

	time.Sleep(time.Second * 10)
}

func TestMutex(t *testing.T) {
	var mutex = sync.Mutex{}
	var m = map[int]interface{}{
		0: 0,
	}
	go func() {
		for {
			// 其他地方读取
			mutex.Lock()
			_ = m[0]
			mutex.Unlock()
			time.Sleep(time.Second)
			fmt.Println("1")
		}
	}()
	// 方式一 循环内
	for i := 0; i < 10000; i++ {
		mutex.Lock()
		// any call1
		mutex.Unlock()
		// any call2

	}
	// 方式一 循环外
	mutex.Lock()
	for i := 0; i < 10000; i++ {
		// any call
		// any call2
	}
	mutex.Unlock()

	signalx.WaitSignal(func() {

	})

}
