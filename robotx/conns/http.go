package conns

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"zhongguo168a.top/mycodes/gocodes/utils/errutil"
)

func NewHttpConn(name string, addr string, onReceive func(data interface{})) (obj *HttpConn) {
	obj = &HttpConn{
		name:      name,
		addr:      addr,
		onReceive: onReceive,
	}
	return
}

type HttpConn struct {
	name string
	// 连接地址
	addr      string
	onReceive func(data interface{})
}

func (mgr *HttpConn) SendHttpJson(addr string, obj interface{}) {
	//hr.send(`${this.addrLogin}/post`, `msg=${msg}&args=${JSON.stringify(data)}`, 'post', 'text');
	go func() {
		defer errutil.Recover(fmt.Sprintf("send http %+v", obj))
		var (
			err       error
			sendbytes []byte
		)
		switch msg := obj.(type) {
		default:
			_ = msg
			data, marerr := json.Marshal(obj)
			if marerr != nil {
				panic(marerr)
			}

			sendbytes = data
		}

		msgName := reflect.TypeOf(obj).Elem().Name()
		urladdr := fmt.Sprintf("%v", addr)

		//resp, err := http.Post(
		//	urladdr,
		//	"application/x-www-form-urlencoded",
		//	strings.NewReader(fmt.Sprintf("msg=%v&args=%v", msgName, data)))

		resp, posterr := http.Post(urladdr, "application/x-www-form-urlencoded", strings.NewReader(base64.StdEncoding.EncodeToString(sendbytes)))
		//resp, posterr := http.PostForm(urladdr, url.Values{"args": {base64.StdEncoding.EncodeToString(sendbytes)}})
		if posterr != nil {
			fmt.Println(posterr)
			return
		}
		defer resp.Body.Close()

		body, readerr := ioutil.ReadAll(resp.Body)
		if readerr != nil {
			//c.Dispatch(fmt.Sprintf(`Response.%v`, msgName), "Failed", readerr)
			return
		}
		_ = body
		_ = msgName
		_ = err
		//c := mgr.robot
		//typName := msgName + "Resp"
		//obj, err = c.cache.NewData(typName)
		//if err != nil {
		//	return
		//}
		//
		//json.Unmarshal(body, obj)
		//
		//c.Dispatch(fmt.Sprintf(`Response.%v.Succeed`, msgName), obj)
		//c.Dispatch(fmt.Sprintf(`Response.%v`, msgName), "Succeed", obj)
	}()
}

//func (mgr *NetManager) SendHttp(addr string, obj interface{}) {
//	//hr.send(`${this.addrLogin}/post`, `msg=${msg}&args=${JSON.stringify(data)}`, 'post', 'text');
//	go func() {
//		defer errutil.Recover(fmt.Sprintf("send http %+v", obj))
//		var (
//			err       error
//			sendbytes []byte
//		)
//
//		switch msg := obj.(type) {
//		case *message.RequestMessage:
//			msgbs, packerr := msg.Pack()
//			if packerr != nil {
//				err = errors.Wrap(packerr, "pack")
//				return
//			}
//			sendbytes = msgbs
//		default:
//			data, marerr := json.Marshal(obj)
//			if marerr != nil {
//				panic(marerr)
//			}
//
//			sendbytes = data
//		}
//
//		msgName := reflect.TypeOf(obj).Elem().Name()
//		urladdr := fmt.Sprintf("%v/%v", addr, "bytes")
//
//		//resp, err := http.Post(
//		//	urladdr,
//		//	"application/x-www-form-urlencoded",
//		//	strings.NewReader(fmt.Sprintf("msg=%v&args=%v", msgName, data)))
//
//		resp, posterr := http.PostForm(urladdr, url.Values{"args": {base64.StdEncoding.EncodeToString(sendbytes)}})
//		if posterr != nil {
//			fmt.Println(posterr)
//			return
//		}
//		defer resp.Body.Close()
//
//		body, readerr := ioutil.ReadAll(resp.Body)
//		if readerr != nil {
//			//c.Dispatch(fmt.Sprintf(`Response.%v`, msgName), "Failed", readerr)
//			return
//		}
//
//		c := mgr.robot
//		typName := msgName + "Resp"
//		obj, err = c.cache.NewData(typName)
//		if err != nil {
//			return
//		}
//
//		json.Unmarshal(body, obj)
//
//		c.Dispatch(fmt.Sprintf(`Response.%v.Succeed`, msgName), obj)
//		c.Dispatch(fmt.Sprintf(`Response.%v`, msgName), "Succeed", obj)
//	}()
//}
