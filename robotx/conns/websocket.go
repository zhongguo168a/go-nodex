package conns

import (
	"fmt"
	"gitee.com/zhongguo168a/go-nodex/nodex/server/message"
	"github.com/pkg/errors"
	"golang.org/x/net/websocket"
	"io"
	"net"
	"sync/atomic"
)

func NewWebSocket(name string, addr string, onReceive func(data interface{})) (obj *WebSocket) {
	obj = &WebSocket{
		name:      name,
		addr:      addr,
		onReceive: onReceive,
	}
	return
}

type WebSocket struct {
	name string
	// 连接地址
	addr string
	//
	seq int16
	//
	conn net.Conn
	// 是否连接
	connected bool
	//
	reqStartTime int64

	lastSeq int32

	onReceive func(data interface{})
}

func (socket *WebSocket) GetName() string {
	return socket.name
}

func (socket *WebSocket) GetAddr() string {
	return socket.addr
}

func (socket *WebSocket) IsConnected() bool {
	return socket.connected
}

func (socket *WebSocket) Connect() (err error) {
	wsconn, dialerr := websocket.Dial(fmt.Sprintf("%v", socket.addr), "", fmt.Sprintf("http://%v/", socket.addr))
	if dialerr != nil {
		return errors.Wrap(dialerr, "dial websocket")
	}
	socket.conn = wsconn

	socket.connected = true
	go socket.read()
	return
}

func (socket *WebSocket) read() {
	for {
		tagData := make([]byte, 1)
		_, err := io.ReadFull(socket.conn, tagData)
		if err != nil {
			goto close
		}
		// 将headData字节流 拆包到msg中
		tag := message.PushTag(tagData[0])
		switch tag {
		case message.PushTag_Response:
			msg := &message.ResponseMessage{}
			err = msg.Unpack(socket.conn)
			if err != nil {
				fmt.Printf("[client] unpack ResponseMessage: err = %s\n", err)
				continue
			}
			fmt.Printf("[client] response[%s]: data = %s\n", msg.Route, msg.Data)

			socket.onReceive(msg)

		case message.PushTag_Push:
			msg := &message.PushMessage{}
			err = msg.Unpack(socket.conn, 0)
			if err != nil {
				fmt.Printf("[client] unpack PushMessage: err = %s\n", err)
				continue
			}
			fmt.Printf("[client] server push: data = %s\n", msg.Data)
		case message.PushTag_SyncCreate:
			msg := &message.SyncCreate{}
			err = msg.Unpack(socket.conn, 0)
			if err != nil {
				fmt.Printf("[client] unpack SyncCreate: err = %s\n", err)
				continue
			}
			fmt.Printf("[client] sync create: key=%v, data = %s\n", msg.Key, msg.Data)
		}
	}

close:
	socket.connected = false

}

func (socket *WebSocket) Send(obj interface{}) {

}

func (socket *WebSocket) SendByte(msg *message.RequestMessage) (err error) {
	//msgbs, packerr := msg.Pack()
	//if packerr != nil {
	//	err = errors.Wrap(packerr, "pack")
	//	return
	//}
	//_, err = socket.conn.Write(msgbs)
	//if err != nil {
	//	socket.connected = false
	//	return errors.Wrap(err, "write")
	//}
	//
	//socket.reqStartTime = time.Now().UnixNano()
	return
}

func (socket *WebSocket) getRemoteName(conn net.Conn) (x string) {
	x = conn.RemoteAddr().String()
	return

}

func (socket *WebSocket) Close() {
	socket.conn.Close()
	return
}

func (socket *WebSocket) nextSeq() int32 {
	atomic.AddInt32(&socket.lastSeq, 1)
	return socket.lastSeq
}
