package robotx

import (
	"fmt"
	"gitee.com/zhongguo168a/go-nodex/robotx/conns"
)

func (robot *Robot) CreateTCPConnection(name string, addr string) (err error) {
	conn := conns.NewTCPSocket(name, addr, robot.onReceive)
	return conn.Connect()
}
func (robot *Robot) CreateWebSocketConnection(name string, addr string) (err error) {
	conn := conns.NewWebSocket(name, addr, robot.onReceive)
	return conn.Connect()
}

func (robot *Robot) SetDefaultConnection(name string) {
	robot.connDefault = name
}

func (robot *Robot) onReceive(data interface{}) {
	fmt.Println("onReceive", data)
}
