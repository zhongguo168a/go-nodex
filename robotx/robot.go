package robotx

import (
	"gitee.com/zhongguo168a/go-nodex/robotx/clicache"
	"sync"
	"zhongguo168a.top/mycodes/gocodes/event"
)

func NewRobot(account string) (cli *Robot) {
	cli = &Robot{
		account: account,
		cache:   clicache.NewClientCache(),
	}

	cli.Dispatcher = event.NewDispatcher(cli)

	return
}

type Robot struct {
	*event.Dispatcher

	account string

	calls sync.Map
	//
	cache *clicache.ClientCache
	//
	userData interface{}

	conns       map[string]IConnection
	connDefault string
}

func (c *Robot) Run() (err error) {
	//c.netMgr.SendByte(&serverz.RequestMessage{
	//	Seq:   1,
	//	Route: "user.Login",
	//	Rows:  []byte(`{"Content":"test"}`),
	//})

	//c.netMgr.SendHttp("http://127.0.0.1:8082", &serverz.RequestMessage{
	//	Seq:   1,
	//	Route: "user.Login",
	//	Rows:  []byte(`{"Content":"test"}`),
	//})

	//c.SendHttpJson("http://127.0.0.1:8082/user/Login", map[string]interface{}{
	//	"Content": "test",
	//})
	return
}

func (c *Robot) GetCache() *clicache.ClientCache {
	return c.cache
}
