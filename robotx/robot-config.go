package robotx

type IConnection interface {
	GetName() string
	GetAddr() string
	OnReceive(func(data interface{}))
}
