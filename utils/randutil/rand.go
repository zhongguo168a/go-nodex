package randutil

import (
	"math/rand"
)

func NewRander() (obj *Rander) {
	obj = &Rander{}
	obj.Rand = rand.New(rand.NewSource(obj.seed))
	return
}

type Rander struct {
	*rand.Rand

	seed int64
}

func (r *Rander) GetSeed() int64 {
	return r.seed
}

func (r *Rander) Seed(val int64) {
	r.seed = val
	r.Rand.Seed(val)
}

func (r *Rander) From(seed int64) {
	r.Seed(seed)
}

func (r *Rander) To() (seed int64) {
	return r.seed
}

var _rc = 0

func (r *Rander) Chance(chance float64) bool {
	if chance >= 1 {
		return true
	}
	if chance >= 0 {
		var n = r.Intn(1024)
		if _rc < 20 {
			//fmt.Println("> record: rand: ", r.seed, ", ", r)
			_rc++
		}
		var c = int(chance * 1024)
		if c >= n {
			return true
		}
	}
	return false
}
