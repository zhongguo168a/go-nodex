package dbx

import (
	sll "github.com/emirpasic/gods/lists/singlylinkedlist"
	"github.com/emirpasic/gods/utils"
)

func NewSaveQueue() (obj *SaveQueue) {
	obj = &SaveQueue{}
	obj.List = sll.New()
	return
}

type SaveQueue struct {
	*sll.List
}

// Element is an entry in the priority queue
type Element struct {
	name     string
	priority int
}

// Comparator function (sort by element's priority value in descending order)
func byPriority(a, b interface{}) int {
	priorityA := a.(*SaveGroup).createTime
	priorityB := a.(*SaveGroup).createTime
	return utils.IntComparator(priorityA, priorityB) // "-" descending order
}
