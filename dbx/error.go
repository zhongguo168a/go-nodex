package dbx

import "gitee.com/zhongguo168a/gocodes/myx/errorx"

var (
	ErrNotFound          = errorx.New("not found")
	ErrDatabaseException = errorx.New("database exception")
)
