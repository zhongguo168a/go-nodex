package test

import (
	"bou.ke/monkey"
	"container/list"
	"context"
	"fmt"
	"gitee.com/zhongguo168a/go-nodex/dbx"
	"gitee.com/zhongguo168a/go-nodex/dbx/mongox"
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/myx/debugx"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"gitee.com/zhongguo168a/gocodes/myx/identx"
	"gitee.com/zhongguo168a/gocodes/myx/slowtime"
	"github.com/smartystreets/goconvey/convey"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestCreate(t *testing.T) {

}

func TestSaveNormal(t *testing.T) {
	convey.Convey("TestSaveNormal", t, func() {
		node := NewTestDocument()
		_ = node.RefSet("_id", identx.GenOne())
		_ = node.RefSet("UpdateTime", int(slowtime.Now().Unix()))
		_ = node.RefSet("CreateIime", int(slowtime.Now().Unix()))

		convey.Convey("delay-create", func() {
			delay := dbx.NewDelayByKey("test")
			{
				delay.Create(node, node.ToMap())
			}
			{
				func() {
					defer debugx.RecoverError(func(err error) {
						delay.Create(node, node.ToMap())
						convey.So(errorx.Has(err, "delay item existed"), convey.ShouldBeTrue)

					})
				}()
			}
			{
				err := delay.Commit()
				convey.So(err, convey.ShouldBeNil)
			}
			{
				err := delay.Save()
				convey.So(err, convey.ShouldBeNil)
			}
			{
				iNode, err := mongox.NewRequestByName(node.DBTable()).
					WithCreator(func() interface{} {
						return NewTestDocument()
					}).
					WithItemHandler(mongox.CacheItemHandler()).
					FindOne(node.DBQuery())
				lastNode := iNode.(*TestDocument)
				convey.So(err, convey.ShouldBeNil)
				convey.So(lastNode.RefGetInt("UpdateTime"), convey.ShouldEqual, node.RefGetInt("UpdateTime"))
				convey.So(lastNode.RefGetInt("CreateIime"), convey.ShouldEqual, node.RefGetInt("CreateIime"))
			}
		})
	})
}

func TestSave(t *testing.T) {
	convey.Convey("TestSaveNormal", t, func() {
		node := NewTestDocument()
		_ = node.RefSet("_id", identx.GenOne())
		_ = node.RefSet("UpdateTime", int(slowtime.Now().Unix()))
		_ = node.RefSet("CreateIime", int(slowtime.Now().Unix()))

		convey.Convey("delay-bulkwirte error", func() {
			delay := dbx.NewDelayByKey("test")

			monkey.PatchInstanceMethod(reflect.TypeOf(&mongo.Collection{}), "BulkWrite",
				func(col *mongo.Collection, ctx context.Context, models []mongo.WriteModel,
					opts ...*options.BulkWriteOptions) (*mongo.BulkWriteResult, error) {
					time.Sleep(time.Second)
					return nil, mongo.BulkWriteException{
						WriteConcernError: nil,
						WriteErrors: []mongo.BulkWriteError{
							{
								WriteError: mongo.WriteError{Index: 0},
							},
						},
						Labels: nil,
					}
				})
			{
				delay.Update(node, datax.M{
					"Error": "1",
				})
			}
			{
				go func() {
					godelay := dbx.NewDelayByKey("test")
					godelay.Update(node, datax.M{
						"Error": "2",
					})
					godelay.Commit()
					monkey.UnpatchAll()
					godelay.Save()
				}()
			}
			{
				err := delay.Commit()
				convey.So(err, convey.ShouldBeNil)
			}
			{
				err := delay.Save()
				convey.So(err, convey.ShouldBeNil)

			}
		})
	})

	time.Sleep(time.Second)
}

func TestSaveTime并发(t *testing.T) {
	wg := sync.WaitGroup{}
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		time.Sleep(time.Millisecond * 10)
		go func() {
			count := 0
			for {
				delay := dbx.NewDelayByKey(identx.GenOne())
				node := NewTestDocument()
				_ = node.RefSet("_id", identx.GenOne())
				_ = node.RefSet("UpdateTime", int(slowtime.Now().Unix()))
				_ = node.RefSet("CreateIime", int(slowtime.Now().Unix()))
				count++
				{
					delay.Create(node, node.ToMap())
				}
				{
					err := delay.Commit()
					if err != nil {
						fmt.Println("commit", err)
						continue
					}
				}
				if count == 200 {
					break
				}

				go func() {
					dbx.GetSaveRoot().SaveByGroup(delay.GetSaveKey())
				}()

				time.Sleep(time.Millisecond)
			}

			wg.Add(-1)
		}()

		go func() {
			count := 0
			for {
				delay := dbx.NewDelayByKey(identx.GenOne())
				node := NewTestDocument()
				_ = node.RefSet("_id", identx.GenOne())
				_ = node.RefSet("UpdateTime", int(slowtime.Now().Unix()))
				_ = node.RefSet("CreateIime", int(slowtime.Now().Unix()))
				count++
				{
					delay.Create(node, node.ToMap())
				}
				{
					err := delay.Commit()
					if err != nil {
						fmt.Println("commit", err)
						continue
					}
				}
				if count == 20 {
					break
				}
				time.Sleep(time.Millisecond * 100)
			}
		}()
	}

	wg.Wait()
}

func TestSaveMutex(t *testing.T) {
	convey.Convey("delay", t, func() {
		convey.Convey("delay-create", func() {
			//monkey.Patch(rankcli.H行会创建, func(args *mo.H行会创建) (reply *mo.H行会创建Resp, err error) {
			//	return &mo.H行会创建Resp{
			//		H行会编号: H行会编号,
			//	}, nil
			//})
			node := NewTestDocument()
			_ = node.RefSet("_id", identx.GenOne())
			_ = node.RefSet("UpdateTime", int(slowtime.Now().Unix()))
			_ = node.RefSet("CreateIime", int(slowtime.Now().Unix()))
			for i := 0; i < 100; i++ {

				go func() {

					delay := dbx.NewDelayByKey("test")
					{
						delay.Create(node, node.ToMap())
					}
					{
						err := delay.Commit()
						if err != nil {
							panic(err)
						}
					}
					{
						err := delay.Save()
						if err != nil {
							panic(err)
						}
					}
				}()
			}
		})
	})

	time.Sleep(time.Second)
}

func TestName(t *testing.T) {
	l := list.New()
	l.PushBack(4)
	l.PushBack(5)
	l.PushBack(6)
	l.PushBack(7)
	l.PushBack(17)
	l.PushBack(27)
	l.PushBack(71)
	l.PushBack(74)
	//fmt.Println(l.Back().Value)
	//fmt.Println(l.Front().Value)
	for p := l.Front(); p != nil; p = p.Next() {

		fmt.Println(p.Value)
		l.Remove(l.Front())
	}
}
