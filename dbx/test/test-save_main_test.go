package test

import (
	"context"
	"fmt"
	"gitee.com/zhongguo168a/go-nodex/dbx"
	"gitee.com/zhongguo168a/go-nodex/dbx/mongox"
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/datax/cachex"
	"gitee.com/zhongguo168a/gocodes/datax/dictx"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
	"testing"
)

func init() {
	dbx.RegisterTable(&mongox.Table{
		Name:       "testdbx",
		Database:   "testdbx",
		PrimaryKey: "_id",
		Indexs: map[int]*mongo.IndexModel{
			0: {
				Keys: bson.D{
					{Key: "_id", Value: mongox.IndexSortOrderAscende},
				},
			},
		},
	})
}

func NewTestDocument() (obj *TestDocument) {
	obj = &TestDocument{}
	obj.IObject = cachex.NewObject(datax.M{})
	return
}

type TestDocument struct {
	cachex.IObject
}

func (e *TestDocument) DBTable() string {
	return "testdbx"
}

func (e *TestDocument) DBIdent() string {
	return e.RefGetString("_id")
}

func (e *TestDocument) DBQuery() map[string]interface{} {
	return map[string]interface{}{
		"_id": e.DBIdent(),
	}
}

func NewTestItem(文档 dbx.IDBModelDelay, 数据 cachex.IObject) (obj *TestItem) {
	obj = &TestItem{}
	obj.数据 = 数据
	obj.文档 = 文档
	return
}

type TestItem struct {
	数据 cachex.IObject
	文档 dbx.IDBModelDelay
}

func (t *TestItem) DBObject() cachex.IObject {
	return t.数据
}

func (t *TestItem) DBModel() dbx.IDBModelDelay {
	return t.文档
}

func (t *TestItem) DBPath() string {
	return "/dbdict"
}

func (t *TestItem) DBIdent() string {
	return t.数据.RefGetString("_id")
}

func NewTestDict(数据 cachex.IObject) (obj *TestDict) {
	obj = &TestDict{}
	obj.IDictionary = dictx.New[string, *TestItem]()
	obj.S数据 = 数据
	return
}

type TestDict struct {
	dictx.IDictionary[string, *TestItem]
	S数据 cachex.IObject
}

func (集合 *TestDict) DBDict() cachex.IObject {
	return 集合.S数据
}

func (集合 *TestDict) DBGet(key string) (interface{}, bool) {
	return 集合.Get(key)
}

func (集合 *TestDict) DBSet(key string, i数据 interface{}) {
	if i数据 != nil {
		集合.Set(key, i数据.(*TestItem))
	} else {
		集合.Set(key, nil)
	}
}

func (集合 *TestDict) DBDelete(key string) {
	集合.Delete(key)
}

func TestMain(m *testing.M) {
	dbx.RegisterDB(&mongox.Database{
		Name: "testdbx",
		Addrs: []*mongox.Addr{
			{IP: "www.mywork.com", Port: "27017", User: "root", Pwd: "lJ3fB7vH2eW2iJ2e"},
		},
		SubMode: mongox.DBSubMode_None,
	})

	{
		igame, _ := dbx.GetDB("testdbx")
		game := igame.(*mongox.Database)
		gameaddr, _ := game.GetAddr("")
		gamecli, _ := gameaddr.GetConnection()
		err := gamecli.Database("testdbx").Drop(context.Background())
		if err != nil {
			fmt.Println(errorx.Wrap(err, "drop testdbx"))
			os.Exit(-1)
		}
	}

	m.Run()

}
