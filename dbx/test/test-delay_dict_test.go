package test

import (
	"gitee.com/zhongguo168a/go-nodex/dbx"
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/datax/cachex"
	"gitee.com/zhongguo168a/gocodes/myx/identx"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDictDeletePath(t *testing.T) {
	createDictTest()

	{
		delay := dbx.NewDelayByKey("test")
		dictItem := dbdict.MustGet("2")
		delay.DictReplaceField(dictItem, "/cset", datax.M{})
		_ = delay.CommitOrRollback()
		saveItem := dbx.GetSaveRoot().GetSaveItem(dbdocument.DBTable() + "/" + dbdocument.DBIdent())
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"2": datax.M{
					"cset": nil,
				},
			},
		}, saveItem.CreateDocument)
	}
	dbx.GetSaveRoot().SaveNow()
	{
		delay := dbx.NewDelayByKey("test")
		dictItem := dbdict.MustGet("2")
		delay.DictReplace(dictItem, datax.M{
			"cset": datax.M{"c3": 1},
		})
		_ = delay.CommitOrRollback()

		saveItem := dbx.GetSaveRoot().GetSaveItem(dbdocument.DBTable() + "/" + dbdocument.DBIdent())
		assert.Equal(t, datax.M{}, saveItem.CreateDocument)
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"2": datax.M{
					"cset": datax.M{"c1": 11},
				},
			}}, saveItem.SetDocument)
	}
	dbx.GetSaveRoot().SaveNow()
	{
		delay := dbx.NewDelayByKey("test")
		delay.DictDelete(dbdict, dbdict.MustGet("1"))
		delay.DictDelete(dbdict, dbdict.MustGet("2"))
		_ = delay.CommitOrRollback()
		saveItem := dbx.GetSaveRoot().GetSaveItem(dbdocument.DBTable() + "/" + dbdocument.DBIdent())
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"1": nil,
				"2": nil,
			},
		}, saveItem.UnsetDocument)

	}
	{
		delay := dbx.NewDelayByKey("test")
		item := NewTestItem(dbdocument, cachex.NewObject(datax.M{"_id": "2"}))
		delay.DictCreateToDBDict(dbdict, item, datax.M{})
		_ = delay.CommitOrRollback()
		saveItem := dbx.GetSaveRoot().GetSaveItem(dbdocument.DBTable() + "/" + dbdocument.DBIdent())
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"2": datax.M{},
			},
		}, saveItem.SetDocument)
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"1": nil,
			},
		}, saveItem.UnsetDocument)
	}
	{
		delay := dbx.NewDelayByKey("test")
		dictItem := dbdict.MustGet("2")
		delay.DictUpdate(dictItem, datax.M{
			"cset": datax.M{
				"c3": 1,
			},
		})
		_ = delay.CommitOrRollback()
		saveItem := dbx.GetSaveRoot().GetSaveItem(dbdocument.DBTable() + "/" + dbdocument.DBIdent())
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"2": datax.M{
					"cset": datax.M{
						"c3": 1,
					},
				},
			},
		}, saveItem.SetDocument)
		assert.Equal(t, datax.M{
			"dbdict": datax.M{
				"1": nil,
			},
		}, saveItem.UnsetDocument)
	}

	//{
	//	item := NewTestItem(dbdocument, cachex.NewObject(datax.M{"_id": "2"}))
	//	delay.DictCreateToDBDict(dbdict, item, datax.M{})
	//	delay.DictCreateByDictPath(item, "/cset", "c1", 1)
	//	//delay.DictCreateByDictPath(item, "/cset", "c2", 2)
	//
	//}

	dbx.GetSaveRoot().SaveNow()

}

func createDictTest() {
	delay := dbx.NewDelayByKey("test")
	dbdocument = NewTestDocument()
	_ = dbdocument.RefSet("_id", identx.GenOne())
	_ = dbdocument.RefSet("string", "string")
	delay.Create(dbdocument, dbdocument.ToMap())

	dbdict = NewTestDict(dbdocument.RefGetObject("dbdict").RefNewIfNil())
	{
		item := NewTestItem(dbdocument, cachex.NewObject(datax.M{"_id": "1"}))
		delay.DictCreateToDBDict(dbdict, item, datax.M{"a": "a", "b": "b"})
	}
	{
		item := NewTestItem(dbdocument, cachex.NewObject(datax.M{"_id": "2"}))
		delay.DictCreateToDBDict(dbdict, item, datax.M{})
		delay.DictUpdate(item, datax.M{
			"cset": datax.M{
				"c1": 1,
				"c2": 2,
			}})

	}
	err := delay.CommitOrRollback()
	if err != nil {
		panic(err)
	}
	dbx.GetSaveRoot().SaveNow()

}

var dbdict *TestDict
var dbdocument *TestDocument
