package dbx

type Opt int

const (
	Opt_创建 Opt = iota + 1
	Opt_修改
	Opt_删除
)

type DictOpt int

const (
	DictOpt_创建 DictOpt = iota + 1
	DictOpt_修改
	DictOpt_删除
	DictOpt_替换
)

func NewDelayItem(table, tableKey string) (obj *DelayItem) {
	obj = &DelayItem{}
	obj.TableKey = tableKey
	obj.Table = table
	obj.Ident = GetDocumentIdent(table, tableKey)
	return
}

// 表示一个被延时修改的映射的数据库对象
type DelayItem struct {
	Opt Opt

	Ident    string
	TableKey string
	Table    string
	// 地址的索引
	Database string
	// 用于查找行
	Query map[string]interface{}
	//
	Data map[string]interface{}
	//
	OptQueue []*OptItem
}

// map扁平化
// a -> b
func walkDocMap(a, b map[string]interface{}, keys string, split string) {
	if split == "" {
		split = "."
	}
	var lastkey string
	for key, val := range a {
		if keys == "" {
			lastkey = key
		} else {
			lastkey = keys + split + key
		}
		switch v := val.(type) {
		case map[string]interface{}:
			if len(v) == 0 {
				b[lastkey] = v
			} else {
				walkDocMap(v, b, lastkey, split)
			}
		default:
			b[lastkey] = val
		}
	}
}

// table + ident
func GetDocumentIdent(table string, tableKey string) string {
	return table + "/" + tableKey
}
func (item *DelayItem) SetOptDelete() {
	item.Opt = Opt_删除
	item.OptQueue = item.OptQueue[1:]
	item.Data = nil
}
