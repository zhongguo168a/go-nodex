package dbx

// 成功提交后，需要自己处理外部的删除逻辑
func (delayQueue *DelayQueue) Delete(model IDBModel) {
	if delayQueue.isExpired {
		panic("delay is expired")
	}

	delayItem := delayQueue.getOrNewItem(model)
	switch delayItem.Opt {
	case Opt_删除:
	case Opt_创建:
		delete(delayQueue.items, model.DBIdent())
	case Opt_修改:
		delayItem.SetOptDelete()
	default:
		delayItem.SetOptDelete()
	}
	return
}

// 删除数据模型内部的集合的键
func (delayQueue *DelayQueue) DictDelete(dbset IDBDict, dictItem IDBDictItem) {
	if delayQueue.isExpired {
		panic("delay is expired")
	}

	path := dictItem.DBPath()
	if string(path[0]) != "/" {
		panic("路径的首字符必须为/")
	}

	key := dictItem.DBIdent()
	_, hasObject := dbset.DBGet(key)
	if hasObject == false { // 对象不存在或者已被删除
		return
	}

	model := dictItem.DBModel()
	delayItem := delayQueue.getOrNewItem(model)
	switch delayItem.Opt {
	case Opt_删除:
		panic("文档已经被删除")
	case Opt_修改:
	case Opt_创建:
	default:
		delayItem.Opt = Opt_修改
	}
	delayItem.addOptItem(&OptItem{
		Opt:   DictOpt_删除,
		Path:  path + "/" + key,
		Field: key,
	})

	// 删除内存中的值
	dict := dbset.DBDict()
	dict.RefRemove(key)
	dbset.DBDelete(key)

	delayQueue.RollbackAfter(func(ctx *AfterHandler) {
		dict.RefSet(key, model.ToMap())
		dbset.DBSet(key, dictItem)
	})
}

// 过期: 删除路径容易导致与更新的冲突，为了避免难以发现的bug，使用update整个object来代替
// 为了避免无效字段过多，可以在角色登陆时，清理掉多余的字段
// 删除数据模型内部的集合的键
func (delayQueue *DelayQueue) _DictDeleteByModelPath(model IDBModelDelay, dictPath string) {
}
