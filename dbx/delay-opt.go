package dbx

type OptItem struct {
	Path  string
	Field string
	Opt   DictOpt
	Data  interface{}
}

func (item *DelayItem) addOptItem(optItem *OptItem) {
	item.OptQueue = append(item.OptQueue, optItem)
}
