package operator

const (
	EQ  = "$eq"
	GT  = "$gt"
	GTE = "$gte"
	IN  = "$in"
)
