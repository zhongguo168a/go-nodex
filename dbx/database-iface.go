package dbx

import (
	"gitee.com/zhongguo168a/gocodes/datax/cachex"
)

// IDBModel 数据模型
type IDBModel interface {
	DBTable() string
	// DBIdent 模型的唯一标识
	DBIdent() string
	// DBQuery 访问该数据的Query语句，通常为编号
	DBQuery() map[string]interface{}
}

// 数据模型
type IDBModelDelay interface {
	IDBModel
	//
	cachex.IObject
}

type IDBSub interface {
	// DBSubkey 数据库的分表键
	DBSubkey() string
}

type IDBTableSub interface {
	// DBTableSubkey 数据表的分表键
	DBTableSubkey() string
}

// IDBName 获取数据库名字
// 模型实现接口后，可以根据实例更换数据库的名字，例如根据不同的服务器分库
type IDBName interface {
	DBName() (name string, err error)
}

// 数据字典
type IDBDict interface {
	DBDict() cachex.IObject // 对应文档中的一个Map结构数据
	DBGet(key string) (interface{}, bool)
	DBSet(key string, val interface{})
	DBDelete(key string)
}

// 数据字典中的项
type IDBDictItem interface {
	// 对应[DBModel]中的一个对象
	DBObject() cachex.IObject
	// 对应mongo数据库的一个文档模型
	DBModel() IDBModelDelay
	// 数据集对象在数据源中的路径
	// 例子: /a/b/c
	DBPath() string
	// 对象的唯一标识
	DBIdent() string
}
