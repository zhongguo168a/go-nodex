package dbx

import (
	"sync"
)

type IDB interface {
	GetName() string
	SaveTables(items []*SaveItem) (fails []*SaveItem, err error)
}

func RegisterDB(db IDB) {
	databases.Store(db.GetName(), db)
}

func RemoveDB(name string) {
	databases.Delete(name)
}

func GetDB(name string) (db IDB, ok bool) {
	idb, ok := databases.Load(name)
	if ok == false {
		return
	}
	db = idb.(IDB)
	return
}

var databases = sync.Map{}

// 数据库异常的时候，阻止用户产生新的数据
var databaseException bool
