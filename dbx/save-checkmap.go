package dbx

import (
	"fmt"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
)

func CheckMap(from, to map[string]interface{}) (err error) {
	err = checkMap(from, to, "/")
	return
}

func checkMap(from, to map[string]interface{}, path string) (err error) {
	for key, val := range from {
		err = checkAny(val, to[key], path+"/"+key)
		if err != nil {
			return
		}
	}
	return
}

func checkAny(fromVal, toVal interface{}, path string) (err error) {
	if toVal == nil {
		return
	}

	switch fv := fromVal.(type) {
	case map[string]interface{}:
		if toVal == nil {
			toVal = map[string]interface{}{}
		}
		switch tv := toVal.(type) {
		case map[string]interface{}:
			err = checkMap(fv, tv, path)
			if err != nil {
				return
			}
		default:
			err = errorx.New(fmt.Sprintf("to value not map: type=%v, path=%v", tv, path))
			return
		}
	case []interface{}:
		// 数组直接覆盖
		_, ok := toVal.([]interface{})
		if ok == false {
			err = errorx.New("to value not array: path=" + path)
			return
		}
		return
	case int:
	case int8:
	case int16:
	case int32:
	case int64:
	case uint:
	case uint8:
	case uint16:
	case uint32:
	case uint64:
	case float32:
	case float64:
	case string:
	case bool:
	default:
		err = errorx.New(fmt.Sprintf("from value not support: type=%T, path=%v", fv, path))
		return

	}
	return
}
