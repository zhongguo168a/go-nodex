package dbx

import (
	"gitee.com/zhongguo168a/gocodes/myx/debugx"
	"gitee.com/zhongguo168a/gocodes/myx/errorx"
	"gitee.com/zhongguo168a/gocodes/myx/identx"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDealyCreate(t *testing.T) {
	delay := NewDelay()
	document := NewTestDocument()
	_ = document.RefSet("_id", identx.GenOne())
	_ = document.RefSet("string", "string")
	_ = document.RefSet("int", 1)
	dict := document.RefGetObject("dict").RefNewIfNil()
	dict.RefSet("1", 1)
	delay.Create(document, document.ToMap())
	assert.Equal(t, 1, len(delay.items))
	_ = delay.CommitOrRollback()
	assert.Equal(t, 0, len(delay.items))
}

func TestDealyCreateAndCreate(t *testing.T) {
	delay := NewDelay()
	document := NewTestDocument()
	_ = document.RefSet("_id", identx.GenOne())
	_ = document.RefSet("string", "string")
	delay.Create(document, document.ToMap())

	func() {
		defer debugx.RecoverError(func(err error) {
			assert.Equal(t, true, errorx.Has(err, "delay item existed"))
		})

		document2 := NewTestDocument()
		_ = document2.RefSet("_id", document.RefGetString("_id"))
		_ = document2.RefSet("string", "string2")
		delay.Create(document2, document2.ToMap())
	}()

	_ = delay.CommitOrRollback()
}

func TestDealyCreateAndDelete(t *testing.T) {
	delay := NewDelay()
	document := NewTestDocument()
	_ = document.RefSet("_id", identx.GenOne())
	_ = document.RefSet("string", "string")
	delay.Create(document, document.ToMap())
	delay.Delete(document)
	assert.Equal(t, 0, len(delay.items))
	delay.Delete(document)
	assert.Equal(t, 1, len(delay.items))
	_ = delay.CommitOrRollback()
}
