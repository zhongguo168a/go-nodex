package dbx

import (
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/datax/cachex"
	"gitee.com/zhongguo168a/gocodes/datax/dictx"
)

func NewTestDocument() (obj *TestDocument) {
	obj = &TestDocument{}
	obj.IObject = cachex.NewObject(datax.M{})
	return
}

type TestDocument struct {
	cachex.IObject
}

func (e *TestDocument) DBTable() string {
	return "testdbx"
}

func (e *TestDocument) DBIdent() string {
	return e.RefGetString("_id")
}

func (e *TestDocument) DBQuery() map[string]interface{} {
	return map[string]interface{}{
		"_id": e.DBIdent(),
	}
}

func NewTestItem(文档 IDBModelDelay, 数据 cachex.IObject) (obj *TestItem) {
	obj = &TestItem{}
	obj.数据 = 数据
	obj.文档 = 文档
	return
}

type TestItem struct {
	数据 cachex.IObject
	文档 IDBModelDelay
}

func (t *TestItem) DBObject() cachex.IObject {
	return t.数据
}

func (t *TestItem) DBModel() IDBModelDelay {
	return t.文档
}

func (t *TestItem) DBPath() string {
	return "/dbdict"
}

func (t *TestItem) DBIdent() string {
	return t.数据.RefGetString("_id")
}

func NewTestDict(数据 cachex.IObject) (obj *TestDict) {
	obj = &TestDict{}
	obj.IDictionary = dictx.New[string, *TestItem]()
	obj.S数据 = 数据
	return
}

type TestDict struct {
	dictx.IDictionary[string, *TestItem]
	S数据 cachex.IObject
}

func (集合 *TestDict) DBDict() cachex.IObject {
	return 集合.S数据
}

func (集合 *TestDict) DBGet(key string) (interface{}, bool) {
	return 集合.Get(key)
}

func (集合 *TestDict) DBSet(key string, i数据 interface{}) {
	if i数据 != nil {
		集合.Set(key, i数据.(*TestItem))
	} else {
		集合.Set(key, nil)
	}
}

func (集合 *TestDict) DBDelete(key string) {
	集合.Delete(key)
}

type TestTable struct {
}

func (t *TestTable) GetDBKind() string {
	return "testdbx"
}

func (t *TestTable) GetName() string {
	return "testdbx"
}

func (t *TestTable) GetSubName(subkey string) string {
	return ""
}

func (t *TestTable) GetDatabase() string {
	return "testdbx"
}
func init() {
	RegisterTable(&TestTable{})
}
