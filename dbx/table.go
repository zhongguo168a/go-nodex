package dbx

import "sync"

// ITable todo: 增加验证模型是否符合分库分表的方法
type ITable interface {
	// 获取数据库类型
	GetDBKind() string
	GetName() string
	GetSubName(subkey string) string
	GetDatabase() string
}

// 源的集合
var dbTables = sync.Map{}

func RegisterTable(source ITable) {
	dbTables.Store(source.GetName(), source)
}

func RemoveTable(name string) {
	dbTables.Delete(name)
}

func GetTable(name string) (source ITable, ok bool) {
	isrc, ok := dbTables.Load(name)
	if ok == false {
		return
	}
	source = isrc.(ITable)
	return
}
