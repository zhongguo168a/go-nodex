package dbx

// todo: 处理需要把map项设置成空的情况
func copyMap(from, to map[string]interface{}) {
	copyMapObj(from, to)
	return
}

func copyMapObj(from, to map[string]interface{}) {
	for key, val := range from {
		to[key] = copyAny(val, to[key])
	}
	return
}

func copyAny(fromVal, toVal interface{}) (r interface{}) {
	switch fv := fromVal.(type) {
	case map[string]interface{}:
		if toVal == nil {
			toVal = map[string]interface{}{}
		}
		switch tv := toVal.(type) {
		case map[string]interface{}:
			if len(fv) == 0 { // 类似于 array.length = 0, 定义成初始化 map
				r = map[string]interface{}{}
			} else {
				if fv != nil {
					if tv == nil {
						tv = map[string]interface{}{}
					}
					copyMapObj(fv, tv)
					r = tv
				}
			}

		default:
			return
		}
	case []interface{}:
		// 数组直接覆盖
		return fv
	default:
		r = fv
	}
	return
}
