package dbx

import (
	"gitee.com/zhongguo168a/gocodes/myx/logx"
	"sync"
)

var InitTableFuncMap = &sync.Map{}

// 初始化数据库表
func Init() {
	dbTables.Range(func(key, value interface{}) bool {
		itable := value.(ITable)
		initTable, has := InitTableFuncMap.Load(itable.GetDBKind())
		if has {
			f := initTable.(func(table ITable) (err error))
			err := f(itable)
			if err != nil {
				logx.Fatal("初始化数据库索引: ", err)
			}

		}
		return true
	})
}

type Config struct {
	// 创建和修改允许使用nil值
	// 如果设置一个字段的值为nil时，会导致该字段所有数据清空
	// 由于本库采用文档的方式保存数据，数据会很集中，万一错误使用nil，会导致数据丢失
	// 因此，所有的方法，都会清理掉参数的nil字段，尽可能避免数据丢失这种严重且无法挽回的错误
	// 如果确实能保证nil的正确使用，可使用【EnableNil】跳过
	EnableNil bool
}
