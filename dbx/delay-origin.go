package dbx

import (
	"gitee.com/zhongguo168a/gocodes/datax"
	"gitee.com/zhongguo168a/gocodes/datax/cachex"
)

type OriginItem struct {
	object   cachex.IObject
	document datax.M
}
