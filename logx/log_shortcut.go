package logx

import (
	"gitee.com/zhongguo168a/gocodes/myx/logx/fmtlog"
	"gitee.com/zhongguo168a/gocodes/myx/logx/iface"
)

type M map[string]interface{}

var logger = fmtlog.NewLogger()

// Debug 使用默认的日志处理器输出
func Debug(msg ...interface{}) {
	logger.Debug(msg...)
}

// DebugFunc 使用默认的日志处理器输出
func DebugFunc(f func() []interface{}) {
	logger.Debug(f()...)
}

// Info 使用默认的日志处理器输出
func Info(msg ...interface{}) {
	logger.Info(msg...)
}

// InfoFunc 使用默认的日志处理器输出
func InfoFunc(f func() []interface{}) {
	logger.Info(f()...)
}

// Warn 使用默认的日志处理器输出
func Warn(msg ...interface{}) {
	logger.Warn(msg...)
}

// Error 使用默认的日志处理器输出
func Error(msg ...interface{}) {
	logger.Error(msg...)
}

// Fatal 使用默认的日志处理器输出
func Fatal(msg ...interface{}) {
	logger.Fatal(msg...)
}

// Ok 使用默认的日志处理器输出
func Ok(msg ...interface{}) {
	logger.Ok(msg...)
}

// Fail 使用默认的日志处理器输出
func Fail(msg ...interface{}) {
	logger.Fail(msg...)
}

// Msg 使用默认的日志处理器输出
func Msg(msg ...interface{}) {
	logger.Msg(msg...)
}

// With 输出时携带的参数
func With(m M) iface.ILogger {
	return logger.With(m)
}
